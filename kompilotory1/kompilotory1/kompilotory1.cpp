﻿#include <iostream>
#include <stack>
#include <string>
#include <cmath>

using namespace std;

bool czyOperator(char c) {
    return c == '+' || c == '-' || c == '*' || c == '/';
}

int ustalPriorytet(char op) {
    if (op == '+' || op == '-')
        return 1;
    if (op == '*' || op == '/')
        return 2;
    return 0;
}

string naONP(const string& wyrazenie) {
    string onp;
    stack<char> operatory;

    for (size_t i = 0; i < wyrazenie.length(); i++) {
        if (wyrazenie[i] == ' ')
            continue;

        if (isdigit(wyrazenie[i])) {
            // Jeśli napotkano cyfrę, dodaj wszystkie kolejne cyfry do wyjścia ONP
            while (i < wyrazenie.length() && isdigit(wyrazenie[i])) {
                onp += wyrazenie[i];
                i++;
            }
            onp += ' ';
            // Zmniejsz i o 1, aby skorygować inkrementację w pętli for
            i--;
        }
        else if (czyOperator(wyrazenie[i])) {
            // Jeśli napotkano operator, porównaj jego priorytet z priorytetem operatorów na stosie
            while (!operatory.empty() && ustalPriorytet(operatory.top()) >= ustalPriorytet(wyrazenie[i])) {
                // Jeśli priorytet operatora na szczycie stosu jest większy lub równy, przesuń go do wyjścia ONP
                onp += operatory.top();
                onp += ' ';
                operatory.pop();
            }
            // Umieść aktualny operator na stosie
            operatory.push(wyrazenie[i]);
        }
    }

    // Dodaj pozostałe operatory ze stosu do wyjścia ONP
    while (!operatory.empty()) {
        onp += operatory.top();
        onp += ' ';
        operatory.pop();
    }

    return onp;
}

double obliczONP(const string& onp) {
    stack<double> wartosci;

    for (size_t i = 0; i < onp.length(); i++) {
        if (isdigit(onp[i])) {
            // Jeśli napotkano cyfrę, przekształć wszystkie kolejne cyfry na liczbę i umieść na stosie
            double wartosc = 0;
            while (i < onp.length() && isdigit(onp[i])) {
                wartosc = wartosc * 10 + (onp[i] - '0');
                i++;
            }
            wartosci.push(wartosc);
            // Zmniejsz i o 1, aby skorygować inkrementację w pętli for
            i--;
        }
        else if (czyOperator(onp[i])) {
            // Jeśli napotkano operator, pobierz dwie wartości ze stosu, wykonaj operację i umieść wynik na stosie
            double b = wartosci.top();
            wartosci.pop();
            double a = wartosci.top();
            wartosci.pop();

            switch (onp[i]) {
            case '+':
                wartosci.push(a + b);
                break;
            case '-':
                wartosci.push(a - b);
                break;
            case '*':
                wartosci.push(a * b);
                break;
            case '/':
                wartosci.push(a / b);
                break;
            }
        }
    }

    // Ostateczny wynik znajduje się na szczycie stosu
    return wartosci.top();
}

int main() {
    while (1<2) {
    string wyrazenie;
    cout << "Wprowadź wyrażenie matematyczne: ";
    getline(cin, wyrazenie);

    string onp = naONP(wyrazenie);
    cout << "Notacja odwrotna polska (ONP): " << onp << endl;

    double wynik = obliczONP(onp);
    cout << "Wynik: " << wynik << endl;
    }
    return 0;
    
}
