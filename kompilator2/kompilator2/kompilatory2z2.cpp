﻿#include <iostream>
#include <regex>
#include <string>

using namespace std;

int main() {
    string ipAddress;
    cout << "Podaj adres IP: ";
    cin >> ipAddress;

    //odpowienie wartosci
    regex ipRegex(R"(^(?:\d{1,3}\.){3}\d{1,3}$)");

    // szukanie odpowiednich wartosci na odpowiednich miejscach w ciagu
    if (regex_match(ipAddress, ipRegex)) {
        cout << "Adres IP jest poprawny." << endl;
    }
    else {
        cout << "Nieprawidłowy adres IP." << endl;
    }

    return 0;
}
