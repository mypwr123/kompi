﻿#include <string>
#include <iostream>
#include <fstream>


using namespace std;

int main() {
	ifstream inputFile("biblia.txt"); // plik wejściowy

	if (!inputFile) {
		cerr << "Nie można otworzyć pliku wejściowego." << endl;
		return 1;
	}

	string line;
	string modifiedLine;
	char previousChar = ' '; // Poprzedni znak na początku to spacja

	while (getline(inputFile, line)) {
		modifiedLine.clear();

		for (char currentChar : line) {
			if ((currentChar == '.' || currentChar == ',') && previousChar != ' ') {
				modifiedLine += ' '; // Wstaw odstęp, jeśli poprzedni znak nie jest spacją
			}

			modifiedLine += currentChar;
			previousChar = currentChar;
		}

		cout << modifiedLine << endl;
	}

	inputFile.close();

	return 0;
}
