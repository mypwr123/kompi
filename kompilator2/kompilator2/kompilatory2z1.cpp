﻿#include <iostream>
#include <regex>

using namespace std;

int main() {

    regex pattern("^0b(1(0)*)?$");

    string input;

    cout << "Podaj liczbe binarna: ";
    cin >> input;

    if (regex_match(input, pattern)) {
        cout << "Liczba jest potega 2 w zapisie binarnym." << endl;
    }
    else {
        cout << "Liczba nie jest potega 2 w zapisie binarnym." << endl;
    }

    return 0;
}
