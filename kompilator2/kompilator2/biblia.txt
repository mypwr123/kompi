,,,, ....
    asd,asd,asdfg.arg  śmieci do zadania 4


Na początku Bóg stworzył niebo i ziemię. 2 Ziemia zaś była bezładem i pustkowiem: ciemność była nad powierzchnią bezmiaru wód, a Duch2 Boży unosił się nad wodami.
3 Wtedy Bóg rzekł: «Niechaj się stanie światłość!» I stała się światłość. 4 Bóg widząc, że światłość jest dobra, oddzielił ją od ciemności. 5 I nazwał Bóg światłość dniem, a ciemność nazwał nocą.
I tak upłynął wieczór i poranek - dzień pierwszy.
6 A potem Bóg rzekł: «Niechaj powstanie sklepienie w środku wód i niechaj ono oddzieli jedne wody od drugich!» 7 Uczyniwszy to sklepienie, Bóg oddzielił wody pod sklepieniem od wód ponad sklepieniem; a gdy tak się stało, 8 Bóg nazwał to sklepienie niebem.
I tak upłynął wieczór i poranek - dzień drugi.
9 A potem Bóg rzekł: «Niechaj zbiorą się wody spod nieba w jedno miejsce i niech się ukaże powierzchnia sucha!» A gdy tak się stało, 10 Bóg nazwał tę suchą powierzchnię ziemią, a zbiorowisko wód nazwał morzem. Bóg widząc, że były dobre, 11 rzekł: «Niechaj ziemia wyda rośliny zielone: trawy dające nasiona, drzewa owocowe rodzące na ziemi według swego gatunku owoce, w których są nasiona». I stało się tak. 12 Ziemia wydała rośliny zielone: trawę dającą nasienie według swego gatunku i drzewa rodzące owoce, w których było nasienie według ich gatunków. A Bóg widział, że były dobre.
13 I tak upłynął wieczór i poranek - dzień trzeci.
14 A potem Bóg rzekł: «Niechaj powstaną ciała niebieskie, świecące na sklepieniu nieba, aby oddzielały dzień od nocy, aby wyznaczały pory roku, dni i lata; 15 aby były ciałami jaśniejącymi na sklepieniu nieba i aby świeciły nad ziemią». I stało się tak. 16 Bóg uczynił dwa duże ciała jaśniejące: większe, aby rządziło dniem, i mniejsze, aby rządziło nocą, oraz gwiazdy. 17 I umieścił je Bóg na sklepieniu nieba, aby świeciły nad ziemią; 18 aby rządziły dniem i nocą i oddzielały światłość od ciemności. A widział Bóg, że były dobre.
19 I tak upłynął wieczór i poranek - dzień czwarty.
20 Potem Bóg rzekł: «Niechaj się zaroją wody od roju istot żywych, a ptactwo niechaj lata nad ziemią, pod sklepieniem nieba!» 21 Tak stworzył Bóg wielkie potwory morskie i wszelkiego rodzaju pływające istoty żywe, którymi zaroiły się wody, oraz wszelkie ptactwo skrzydlate różnego rodzaju. Bóg widząc, że były dobre, 22 pobłogosławił je tymi słowami: «Bądźcie płodne i mnóżcie się, abyście zapełniały wody morskie, a ptactwo niechaj się rozmnaża na ziemi».
23 I tak upłynął wieczór i poranek - dzień piąty.
24 Potem Bóg rzekł: «Niechaj ziemia wyda istoty żywe różnego rodzaju: bydło, zwierzęta pełzające i dzikie zwierzęta według ich rodzajów!» I stało się tak. 25 Bóg uczynił różne rodzaje dzikich zwierząt, bydła i wszelkich zwierząt pełzających po ziemi. I widział Bóg, że były dobre. 26 3A wreszcie rzekł Bóg: «Uczyńmy człowieka na Nasz obraz, podobnego Nam. Niech panuje nad rybami morskimi, nad ptactwem powietrznym, nad bydłem, nad ziemią i nad wszystkimi zwierzętami pełzającymi po ziemi!»
27 Stworzył więc Bóg człowieka na swój obraz,
na obraz Boży go stworzył:
stworzył mężczyznę i niewiastę.
28 Po czym Bóg im błogosławił, mówiąc do nich: «Bądźcie płodni i rozmnażajcie się, abyście zaludnili ziemię i uczynili ją sobie poddaną; abyście panowali nad rybami morskimi, nad ptactwem powietrznym i nad wszystkimi zwierzętami pełzającymi po ziemi». 29 I rzekł Bóg: «Oto wam daję wszelką roślinę przynoszącą ziarno po całej ziemi i wszelkie drzewo, którego owoc ma w sobie nasienie: dla was będą one pokarmem. 30 A dla wszelkiego zwierzęcia polnego i dla wszelkiego ptactwa w powietrzu, i dla wszystkiego, co się porusza po ziemi i ma w sobie pierwiastek życia, będzie pokarmem wszelka trawa zielona». I stało się tak. 31 A Bóg widział, że wszystko, co uczynił, było bardzo dobre.
I tak upłynął wieczór i poranek - dzień szósty.

1 W ten sposób zostały ukończone niebo i ziemia oraz wszystkie jej zastępy [stworzeń].
2 A gdy Bóg ukończył w dniu szóstym swe dzieło, nad którym pracował, odpoczął1 dnia siódmego po całym swym trudzie, jaki podjął. 3 Wtedy Bóg pobłogosławił ów siódmy dzień i uczynił go świętym; w tym bowiem dniu odpoczął po całej swej pracy, którą wykonał stwarzając.
4 Oto są dzieje początków po stworzeniu nieba i ziemi.

Drugi opis stworzenia człowieka

Gdy Pan2 Bóg uczynił ziemię i niebo, 5 nie było jeszcze żadnego krzewu polnego na ziemi, ani żadna trawa polna jeszcze nie wzeszła - bo Pan Bóg nie zsyłał deszczu na ziemię i nie było człowieka, który by uprawiał ziemię 6 i rów kopał w ziemi3, aby w ten sposób nawadniać całą powierzchnię gleby - 7 wtedy to Pan Bóg ulepił człowieka z prochu ziemi i tchnął w jego nozdrza tchnienie życia4, wskutek czego stał się człowiek istotą żywą.

Pierwotny stan szczęścia

8 A zasadziwszy ogród w Eden5 na wschodzie, Pan Bóg umieścił tam człowieka, którego ulepił. 9 Na rozkaz Pana Boga wyrosły z gleby wszelkie drzewa miłe z wyglądu i smaczny owoc rodzące oraz drzewo życia w środku tego ogrodu i drzewo poznania dobra i zła6.
10 Z Edenu zaś wypływała rzeka, aby nawadniać ów ogród, i stamtąd się rozdzielała, dając początek czterem rzekom7. 11 Nazwa pierwszej - Piszon; jest to ta, która okrąża cały kraj Chawila, gdzie się znajduje złoto. 12 A złoto owej krainy jest znakomite; tam jest także wonna żywica i kamień czerwony. 13 Nazwa drugiej rzeki - Gichon; okrąża ona cały kraj - Kusz. 14 Nazwa rzeki trzeciej - Chiddekel; płynie ona na wschód od Aszszuru. Rzeka czwarta - to Perat.
15 Pan Bóg wziął zatem człowieka i umieścił go w ogrodzie Eden, aby uprawiał go i doglądał. 16 A przy tym Pan Bóg dał człowiekowi taki rozkaz: «Z wszelkiego drzewa tego ogrodu możesz spożywać według upodobania; 17 ale z drzewa poznania dobra i zła nie wolno ci jeść, bo gdy z niego spożyjesz, niechybnie umrzesz»8.
18 Potem Pan Bóg rzekł: «Nie jest dobrze, żeby mężczyzna był sam, uczynię mu zatem odpowiednią dla niego pomoc». 19 Ulepiwszy z gleby wszelkie zwierzęta lądowe i wszelkie ptaki powietrzne, Pan Bóg przyprowadził je do mężczyzny, aby przekonać się, jaką on da im nazwę. Każde jednak zwierzę, które określił mężczyzna, otrzymało nazwę "istota żywa"9. 20 I tak mężczyzna dał nazwy wszelkiemu bydłu, ptakom powietrznym i wszelkiemu zwierzęciu polnemu, ale nie znalazła się pomoc odpowiednia dla mężczyzny10.
21 Wtedy to Pan sprawił, że mężczyzna pogrążył się w głębokim śnie, i gdy spał wyjął jedno z jego żeber11, a miejsce to zapełnił ciałem. 22 Po czym Pan Bóg z żebra, które wyjął z mężczyzny, zbudował niewiastę. A gdy ją przyprowadził do mężczyzny, 23 mężczyzna powiedział:
«Ta dopiero jest kością z moich kości i ciałem z mego ciała!
Ta będzie się zwała niewiastą, bo ta z mężczyzny została wzięta»12.
24 Dlatego to mężczyzna opuszcza ojca swego i matkę swoją i łączy się ze swą żoną tak ściśle, że stają się jednym ciałem13.
25 Chociaż mężczyzna i jego żona byli nadzy, nie odczuwali wobec siebie wstydu.

1 A wąż1 był bardziej przebiegły niż wszystkie zwierzęta lądowe, które Pan Bóg stworzył. On to rzekł do niewiasty: «Czy rzeczywiście Bóg powiedział: Nie jedzcie owoców ze wszystkich drzew tego ogrodu?» 2 Niewiasta odpowiedziała wężowi: «Owoce z drzew tego ogrodu jeść możemy, 3 tylko o owocach z drzewa, które jest w środku ogrodu, Bóg powiedział: Nie wolno wam jeść z niego, a nawet go dotykać, abyście nie pomarli». 4 Wtedy rzekł wąż do niewiasty: «Na pewno nie umrzecie! 5 Ale wie Bóg, że gdy spożyjecie owoc z tego drzewa, otworzą się wam oczy i tak jak Bóg będziecie znali dobro i zło».
6 Wtedy niewiasta spostrzegła, że drzewo to ma owoce dobre do jedzenia, że jest ono rozkoszą dla oczu i że owoce tego drzewa nadają się do zdobycia wiedzy. Zerwała zatem z niego owoc, skosztowała i dała swemu mężowi, który był z nią: a on zjadł. 7 A wtedy otworzyły się im obojgu oczy i poznali, że są nadzy; spletli więc gałązki figowe i zrobili sobie przepaski.
8 Gdy zaś mężczyzna i jego żona usłyszeli kroki Pana Boga przechadzającego się po ogrodzie, w porze kiedy był powiew wiatru, skryli się przed Panem Bogiem wśród drzew ogrodu. 9 Pan Bóg zawołał na mężczyznę i zapytał go: «Gdzie jesteś?» 10 On odpowiedział: «Usłyszałem Twój głos w ogrodzie, przestraszyłem się, bo jestem nagi, i ukryłem się». 11 Rzekł Bóg: «Któż ci powiedział, że jesteś nagi? Czy może zjadłeś z drzewa, z którego ci zakazałem jeść?» 12 Mężczyzna odpowiedział: «Niewiasta, którą postawiłeś przy mnie, dała mi owoc z tego drzewa i zjadłem». 13 Wtedy Pan Bóg rzekł do niewiasty: «Dlaczego to uczyniłaś?» Niewiasta odpowiedziała: «Wąż mnie zwiódł i zjadłam». 14 Wtedy Pan Bóg rzekł do węża:
«Ponieważ to uczyniłeś,
bądź przeklęty wśród wszystkich zwierząt domowych i polnych;
na brzuchu będziesz się czołgał i proch będziesz jadł po wszystkie dni twego istnienia.
15 Wprowadzam nieprzyjaźń między ciebie i niewiastę,
pomiędzy potomstwo twoje a potomstwo jej:
ono zmiażdży2 ci głowę,
a ty zmiażdżysz2 mu piętę».
16 Do niewiasty powiedział: «Obarczę cię niezmiernie wielkim trudem twej brzemienności, w bólu będziesz rodziła dzieci, ku twemu mężowi będziesz kierowała swe pragnienia, on zaś będzie panował nad tobą».
17 Do mężczyzny zaś [Bóg] rzekł: «Ponieważ posłuchałeś swej żony i zjadłeś z drzewa, co do którego dałem ci rozkaz w słowach: Nie będziesz z niego jeść -
przeklęta3 niech będzie ziemia z twego powodu:
w trudzie będziesz zdobywał od niej pożywienie dla siebie
po wszystkie dni twego życia.
18 Cierń i oset będzie ci ona rodziła,
a przecież pokarmem twym są płody roli.
19 W pocie więc oblicza twego
będziesz musiał zdobywać pożywienie,
póki nie wrócisz do ziemi,
z której zostałeś wzięty;
bo prochem jesteś
i w proch się obrócisz!»4
20 Mężczyzna dał swej żonie imię Ewa5, bo ona stała się matką wszystkich żyjących.
21 Pan Bóg sporządził dla mężczyzny i dla jego żony odzienie ze skór i przyodział ich. 22 Po czym Pan Bóg rzekł: «Oto człowiek stał się taki jak My: zna dobro i zło; niechaj teraz nie wyciągnie przypadkiem ręki, aby zerwać owoc także z drzewa życia, zjeść go i żyć na wieki»6. 23 Dlatego Pan Bóg wydalił go z ogrodu Eden, aby uprawiał tę ziemię, z której został wzięty. 24 Wygnawszy zaś człowieka, Bóg postawił przed ogrodem Eden cherubów i połyskujące ostrze miecza, aby strzec drogi do drzewa życia7.

1 Mężczyzna zbliżył się do swej żony Ewy. A ona poczęła i urodziła Kaina, i rzekła: «Otrzymałam mężczyznę od Pana». 2 A potem urodziła jeszcze Abla2, jego brata.
Abel był pasterzem trzód, a Kain uprawiał rolę. 3 Gdy po niejakim czasie Kain składał dla Pana w ofierze płody roli, 4 zaś Abel składał również pierwociny ze swej trzody i z ich tłuszczu, Pan wejrzał na Abla i na jego ofiarę; 5 na Kaina zaś i na jego ofiarę nie chciał patrzeć. Smuciło to Kaina bardzo i chodził z ponurą twarzą. 6 3 Pan zapytał Kaina: «Dlaczego jesteś smutny i dlaczego twarz twoja jest ponura? 7 Przecież gdybyś postępował dobrze, miałbyś twarz pogodną; jeżeli zaś nie będziesz dobrze postępował, grzech leży u wrót i czyha na ciebie, a przecież ty masz nad nim panować».
8 Rzekł Kain do Abla, brata swego: «Chodźmy na pole4». A gdy byli na polu, Kain rzucił się na swego brata Abla i zabił go. 9 Wtedy Bóg zapytał Kaina: «Gdzie jest brat twój, Abel?» On odpowiedział: «Nie wiem. Czyż jestem stróżem brata mego?» 10 Rzekł Bóg: «Cóżeś uczynił? Krew brata twego głośno woła ku mnie z ziemi! 11 Bądź więc teraz przeklęty na tej roli, która rozwarła swą paszczę, aby wchłonąć krew brata twego, przelaną przez ciebie. 12 Gdy rolę tę będziesz uprawiał, nie da ci już ona więcej plonu. Tułaczem i zbiegiem będziesz na ziemi!» 13 Kain rzekł do Pana: «Zbyt wielka jest kara moja, abym mógł ją znieść. 14 Skoro mnie teraz wypędzasz z tej roli, i mam się ukrywać przed tobą, i być tułaczem i zbiegiem na ziemi, każdy, kto mnie spotka, będzie mógł mnie zabić!» 15 Ale Pan mu powiedział: «O, nie! Ktokolwiek by zabił Kaina, siedmiokrotną pomstę poniesie!» Dał też Pan znamię Kainowi, aby go nie zabił, ktokolwiek go spotka5.
16 Po czym Kain odszedł od Pana i zamieszkał w kraju Nod, na wschód od Edenu.

Potomkowie Kaina

17 Kain zbliżył się do swej żony, a ona poczęła i urodziła Henocha6. Gdy Kain zbudował miasto, nazwał je imieniem swego syna: Henoch.
18 Henoch był ojcem Irada, Irad ojcem Mechujaela, a Mechujael ojcem Metuszaela, Metuszael zaś Lameka7.
19 Lamek wziął sobie dwie żony. Imię jednej było Ada, a drugiej - Silla. 20 Ada urodziła Jabala; on to był praojcem mieszkających pod namiotami i pasterzy. 21 Brat jego nazywał się Jubal; od niego to pochodzą wszyscy grający na cytrze i na flecie. 22 Silla - ona też urodziła Tubal-Kaina; był on kowalem, sporządzającym wszelkie narzędzia z brązu i z żelaza. Siostrą Tubal-Kaina była Naama.
23 Lamek rzekł do swych żon, Ady i Silli:
«Słuchajcie, co wam powiem, żony Lameka.
Nastawcie ucha na moje słowa:
Gotów jestem zabić człowieka dorosłego, jeśli on mnie zrani,
i dziecko - jeśli mi zrobi siniec!
24 Jeżeli Kain miał być pomszczony siedmiokrotnie,
to Lamek siedemdziesiąt siedem razy!»8

Set i jego potomstwo

25 Adam raz jeszcze zbliżył się do swej żony i ta urodziła mu syna, któremu dała imię Set9, «gdyż - jak mówiła - dał mi Bóg potomka innego w zamian za Abla, którego zabił Kain». 26 Setowi również urodził się syn; Set dał mu imię Enosz.
Wtedy zaczęto wzywać imienia Pana10.

1 Oto rodowód potomków Adama.
Gdy Bóg stworzył człowieka, na podobieństwo Boga stworzył go; 2 stworzył mężczyznę i niewiastę, pobłogosławił ich i dał im nazwę "ludzie", wtedy gdy ich stworzył.
3 Gdy Adam miał sto trzydzieści lat, urodził mu się syn, podobny do niego jako jego obraz, i dał mu na imię Set. 4 A po urodzeniu się Seta żył Adam osiemset lat i miał synów oraz córki. 5 Ogólna liczba lat, które Adam przeżył, była dziewięćset trzydzieści. I umarł.
6 Gdy Set miał sto pięć lat, urodził mu się syn Enosz. 7 A po urodzeniu się Enosza żył osiemset siedem lat i miał synów oraz córki. 8 I umarł Set, przeżywszy ogółem dziewięćset dwanaście lat.
9 Gdy Enosz miał dziewięćdziesiąt lat, urodził mu się syn Kenan. 10 I żył Enosz po urodzeniu się Kenana osiemset piętnaście lat, i miał synów oraz córki. 11 Enosz umarł, przeżywszy ogółem dziewięćset pięć lat.
12 Gdy Kenan miał lat siedemdziesiąt, urodził mu się Mahalaleel. 13 A po urodzeniu mu się Mahalaleela żył Kenan osiemset czterdzieści lat i miał synów i córki.
14 I gdy Kenan przeżył ogółem dziewięćset dziesięć lat, umarł.
15 Gdy Mahalaleel miał sześćdziesiąt pięć lat, urodził mu się syn Jered. 16 A po urodzeniu się Jereda żył osiemset trzydzieści lat i miał synów i córki. 17 Gdy Mahalaleel miał ogółem osiemset dziewięćdziesiąt pięć lat, umarł.
18 Gdy Jered miał sto sześćdziesiąt dwa lata, urodził mu się syn Henoch. 19 A po urodzeniu się Henocha Jered żył osiemset lat i miał synów i córki. 20 Jered przeżył ogółem dziewięćset sześćdziesiąt dwa lata, i umarł.
21 Gdy Henoch miał sześćdziesiąt pięć lat, urodził mu się syn Metuszelach. 22 Henoch po urodzeniu się Metuszelacha żył w przyjaźni z Bogiem trzysta lat i miał synów i córki. 23 Ogólna liczba lat życia Henocha: trzysta sześćdziesiąt pięć. 24 Żył więc Henoch w przyjaźni z Bogiem, a następnie znikł, bo zabrał go Bóg2.
25 Gdy Metuszelach miał sto osiemdziesiąt siedem lat, urodził mu się syn Lamek. 26 Po urodzeniu się Lameka żył jeszcze siedemset osiemdziesiąt dwa lata i miał synów i córki. 27 Metuszelach umarł mając ogółem dziewięćset sześćdziesiąt dziewięć lat.
28 Gdy Lamek miał sto osiemdziesiąt dwa lata, urodził mu się syn. 29 A dając mu imię Noe3, powiedział: «Ten niechaj nam będzie pociechą w naszej pracy i trudzie rąk naszych na ziemi, którą Pan przeklął». 30 Lamek po urodzeniu się Noego żył pięćset dziewięćdziesiąt pięć lat i miał synów i córki. 31 Umierając Lamek miał ogółem siedemset siedemdziesiąt siedem lat.
32 A gdy Noe miał pięćset lat, urodzili mu się: Sem4, Cham i Jafet.