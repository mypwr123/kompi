from collections import Counter
import re

# Wczytanie pliku
with open('biblia.txt', 'r', encoding='utf-8') as file:
    text = file.read()

# Usunięcie znaków specjalnych i podział tekstu na słowa
words = re.findall(r'\b\w+\b', text.lower(), re.UNICODE)

# Obliczenie częstości występowania słów
word_counts = Counter(words)

# Pobranie 10 najczęściej występujących słów
top_10_words = word_counts.most_common(10)

# Wyświetlenie wyników
print("10 najczęściej występujących słów w pliku 'biblia.txt':")
for word, count in top_10_words:
    print(f'{word}: {count}')
