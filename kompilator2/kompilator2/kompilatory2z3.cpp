﻿#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main() {
	ifstream inputFile("biblia.txt"); // plik wejsciowy

	string line;
	int lineNumber = 1;

	while (true) {
		char c;
		inputFile.get(c); // Odczytaj  znak z pliku

		if (!inputFile)
			break;

		if (c == '\n') { // Jeśli znaleziono znak nowej linii
			cout << lineNumber << ": " << line << endl; // Wypisz linię z numerem linii
			lineNumber++;
			line.clear(); // Wyczyść zawartość aktualnej linii
		}
		else {
			line += c; // Dodaj odczytany znak do aktualnej linii
		}
	}

	inputFile.close();

	return 0;
}
