
def parse_sql_query(query):
    tokens = query.split()
    query_type = tokens[0].upper()

    if query_type == "WYBIERZ":
        return parse_select_query(tokens)
    elif query_type == "USUŃ":
        return parse_delete_query(tokens)
    else:
        raise ValueError("nie dziala 1")

def parse_select_query(tokens):
    if "Z_TABELI" not in tokens:
        raise ValueError("nie dziala 2")

    select_clause = []
    table = None
    sort_by = None
    where_condition = None

    i = 1
    while i < len(tokens):
        token = tokens[i].upper()

        if token == "Z_TABELI":
            table = tokens[i + 1]
            i += 1
        elif token == "POSORTOWANE_WG":
            sort_by = tokens[i + 1]
            i += 1
        elif token == "GDZIE":
            where_condition = " ".join(tokens[i + 1:])
            break
        else:
            select_clause.append(token)

        i += 1

    select_clause = ", ".join(select_clause)
    query = f"SELECT {select_clause} FROM {table}"

    if where_condition:
        query += f" WHERE {where_condition}"

    if sort_by:
        query += f" ORDER BY {sort_by}"

    return query

def parse_delete_query(tokens):
    if "Z_TABELI" not in tokens:
        raise ValueError("Niepoprawne zapytanie DELETE")

    table = tokens[tokens.index("Z_TABELI") + 1]
    where_condition = None

    if "GDZIE" in tokens:
        where_condition = " ".join(tokens[tokens.index("GDZIE") + 1:])

    query = f"DELETE FROM {table}"

    if where_condition:
        query += f" WHERE {where_condition}"

    return query

# Przykładowe zapytania SQL 
query1 = "WYBIERZ indeks, nazwisko, imię Z_TABELI Studenci POSORTOWANE_WG imię"
query2 = "WYBIERZ indeks, nazwisko, imię Z_TABELI Studenci GDZIE nazwisko ODPOWIADA_WZORCOWI D* POSORTOWANE_WG imię"
query3 = "USUŃ Z_TABELI Studenci GDZIE imię='Stanisław'"

try:
    parsed_query1 = parse_sql_query(query1)
    print("Zapytanie 1:", parsed_query1)

    parsed_query2 = parse_sql_query(query2)
    print("Zapytanie 2:", parsed_query2)

    parsed_query3 = parse_sql_query(query3)
    print("Zapytanie 3:", parsed_query3)

except ValueError as e:
    print("nie dziala3", str(e))
